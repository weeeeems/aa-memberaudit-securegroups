��            )   �      �  &   �  &   �  8   �  	        "     &  
   F  
   Q  7   \     �  3   �     �  0   �     #  (   C     l      �  ?   �  A   �  2   '  *   Z  >   �  A   �       :     6   R  D   �  6   �      �   	  -   �	  O   �	  )   *
  
   T
  3   _
     �
     �
  p   �
     3  :   M  &   �  Y   �  U   	  �   _  4     C   R  u   �  r     S     j   �  x   >  �   �  /   S  �   �  }   	  �   �  �   !                                                                                                    	      
                                Active character:  Active characters:  Activity [Last {inactivity_threshold}] All characters have been added to {MEMBERAUDIT_APP_NAME} Alts only Any Character age [{age_threshold}] Compliance Mains only Maximum allowable inactivity, in <strong>days</strong>. Member Audit Asset Member Audit Skill Points [{skill_point_threshold}] Member Audit Skill Set Minimum allowable age, in <strong>days</strong>. Minimum allowable skill points. Missing character:  Missing characters:  Please create a filter! Please create an audit function! Specify the type of character that needs to have the skill set. The character with the role must be in one of these corporations. The filter description that is shown to end users. User must have a character with this role. User must possess <strong>one</strong> of the selected assets. When True, the filter will also include the users alt-characters. corporation role {inactivity_threshold:d} day {inactivity_threshold:d} days {self.age_threshold:d} day {self.age_threshold:d} days {self.inactivity_threshold:d} day {self.inactivity_threshold:d} days {sp_threshold} skill point {sp_threshold} skill points Project-Id-Version: Russian (Alliance Auth Apps)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Russian <https://weblate.ppfeufer.de/projects/alliance-auth-apps/aa-member-audit-secure-groups/ru/>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 3 : (n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Weblate 5.5.3
 Активный персонаж:  Активные персонажи:  Активные персонажи:  Активные персонажи:  Активность [{inactivity_threshold}] Все персонажи были добавлены в {MEMBERAUDIT_APP_NAME} Только альтернативный Любой Возраст персонажа [{age_threshold}] Согласие Только основной Максимальное допустимое время неактивности, в <strong>днях</strong>. Аудит ассетов Аудит очков опыта [{skill_point_threshold}] Аудит наборов умений Минимальный допустимый возраст, в <strong>днях</strong>. Минимально допустимое количество очков опыта. Отсутствующие персонажи:  Отсутствующие персонажи:  Отсутствующие персонажи:  Отсутствующий персонаж:  Пожалуйста, создайте фильтр! Пожалуйста, создайте функцию аудита! Выберите тип персонажа, который должен обладать набором умений. Персонаж с полномочием должен быть в одной из этих корпораций. Описание фильтра, отображаемое пользователю. У пользователя должен быть персонаж с данным полномочием. Пользователь должен владеть <strong>одним</strong> из выбранных ассетов. Когда Истина - фильтр будет также учитывать альтернативных персонажей пользователя. корпоративные полномочия {inactivity_threshold:d} день {inactivity_threshold:d} дня {inactivity_threshold:d} дней {inactivity_threshold:d} день {self.age_threshold:d} день {self.age_threshold:d} дня {self.age_threshold:d} дней {self.age_threshold:d} день {self.inactivity_threshold:d} день {self.inactivity_threshold:d} дня {self.inactivity_threshold:d} дней {self.inactivity_threshold:d} день {sp_threshold} очко опыта {sp_threshold} очка опыта {sp_threshold} очков опыта {sp_threshold} очко опыта 